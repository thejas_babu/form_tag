Rails.application.routes.draw do
 root "articles#index"
 get '/articles' => "articles#index"
 get 'articles/show' => "articles#show"
 get 'articles/new' => "articles#new"
 post 'articles/create[:id]' => "articles#create"
end
